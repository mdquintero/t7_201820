package test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.RedBlackBST;
import model.vo.Bike;

public class TestArbols {

	private RedBlackBST<Integer, Bike> arbol;
	Bike bici1;
	Bike bici2;
	Bike bici3;
	Bike bici4;
	Bike bici5;
	@Test
	public void test() {
		setUp();
		
		arbol.put(bici1.getBikeId(),bici1);
		arbol.put(bici2.getBikeId(),bici2);
		arbol.put(bici3.getBikeId(),bici3);
		arbol.put(bici4.getBikeId(),bici4);
		arbol.put(bici5.getBikeId(),bici5);
		
		assertEquals("No se a�adieron los elementos correctamente", 5,arbol.size());
		
		
		int ID = bici1.getBikeId();
		int busqueda = arbol.get(1).getBikeId();
		assertEquals("La busqueda/adici�n se encuentra mal", ID, busqueda);
		ID = bici2.getBikeId();
		busqueda = arbol.get(2).getBikeId();
		assertEquals("La busqueda/adici�n se encuentra mal", ID, busqueda);
		ID = bici3.getBikeId();
		busqueda = arbol.get(3).getBikeId();
		assertEquals("La busqueda/adici�n se encuentra mal", ID, busqueda);
		ID = bici4.getBikeId();
		busqueda = arbol.get(4).getBikeId();
		assertEquals("La busqueda/adici�n se encuentra mal", ID, busqueda);
		ID = bici5.getBikeId();
		busqueda = arbol.get(5).getBikeId();
		assertEquals("La busqueda/adici�n se encuentra mal", ID, busqueda);
		
		arbol.delete(1);
		arbol.delete(2);
		arbol.delete(3);
		arbol.delete(4);
		arbol.delete(5);
		assertEquals("No borra los elementos",0,arbol.size());
	}
	private void setUp()
	{
		arbol = new RedBlackBST<>();
		bici1 = new Bike(1,1,1,1);
		bici2 = new Bike(2,2,2,2);
		bici3 = new Bike(3,3,3,3);
		bici4 = new Bike(4,4,4,4);
		bici5 = new Bike(5,5,5,5);
	}
}
