package view;

import java.time.LocalDateTime;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.LinkedList;
import model.logic.Manager;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;
import model.vo.Trip.Tipo;

public class View {

	public static void main(String[] args){

		Controller control = new Controller();
		Scanner linea = new Scanner(System.in);
		boolean fin = false; 
		while(!fin)
		{
			//Muestra cual fuente de datos va a cargar
			printMenu();

			int option = linea.nextInt();
			switch(option)
			{

			case 1:  //Carga de datos 

				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.currentTimeMillis();

				Controller.cargarStations(Manager.STATIONS_Q1_Q2);
				Controller.cargar(Manager.TRIPS_Q1);
				Controller.cargar(Manager.TRIPS_Q2);

				//Tiempo en cargar
				long endTime = System.currentTimeMillis();
				long duration = endTime - startTime;

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");
				break;

			case 2:
				//Memoria y tiempo
				long memoryBeforeCase2 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime2 = System.currentTimeMillis();

				Controller.cargarStations(Manager.STATIONS_Q3_Q4);
				Controller.cargar(Manager.TRIPS_Q3);
				Controller.cargar(Manager.TRIPS_Q4);

				//Tiempo en cargar
				long endTime2 = System.currentTimeMillis();
				long duration2 = endTime2 - startTime2;

				//Memoria usada
				long memoryAfterCase2 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration2 + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase2 - memoryBeforeCase2)/1000000.0) + " MB");
				break;
				
			case 3:
				
				long startTime3 = System.currentTimeMillis();
				
				Controller.cargarBikesRojoNegro();
				
				long endTime3 = System.currentTimeMillis();
				long duration3 = endTime3 - startTime3;
				System.out.println("Se ha cargado el �rbol correctamente \nTiempo en cargar: " + duration3 + " milisegundos");
				break;
				
			case 4:
				System.out.println("Ingrese un ID");
				option = linea.nextInt();
				Controller.requerimiento1(option);
				break;
				
			case 5:
				System.out.println("Ingrese un ID M�nimo");
				int ID1 = linea.nextInt();
				System.out.println("Ingrese un ID M�ximo");
				int ID2 = linea.nextInt();
				Controller.requerimiento2(ID1,ID2);
				break; 
				
			case 6: //Salir
				fin = true;
				linea.close();
				break;
			}
		}
	}

	private static void printMenu()
	{
		System.out.println("---------ISIS 1206 - Estructuras de Datos----------");
		System.out.println("-------------------- Taller 5   - 2018-2 ----------------------");
		System.out.println("Iniciar la Fuente de Datos a Consultar :");
		System.out.println("1. Actualizar la informacion del sistema con las fuentes de datos 2017-Q1 y 2017-Q2");
		System.out.println("2. Actualizar la informacion del sistema con las fuentes de datos 2017-Q3 y 2017-Q4");
		System.out.println("3. Crear un �rbol rojo-negro para las bicicletas a partir de la informaci�n del sistema");
		System.out.println("4. Buscar bicicleta por ID");
		System.out.println("5. Buscar bicicletas por rango de IDs");
		System.out.println("6. salir");


	}


	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fecha, String hora)
	{
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[1]);
		int dia = Integer.parseInt(datosFecha[0]);
		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);
		int segundos = Integer.parseInt(datosHora[2]);

		return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
	}

}
