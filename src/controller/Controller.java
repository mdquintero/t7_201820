package controller;

import java.time.LocalDateTime;

import API.IManager;
import model.data_structures.LinkedList;
import model.logic.Manager;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;


public class Controller {
    private static Manager manager = new Manager();

  
    public static void cargar(String dataTrips){
    	manager.cargar(dataTrips);
    }
    
    public static void cargarStations(String rutaStations) {
    	manager.cargarStations(rutaStations);
    }
    
    public static void cargarBikesRojoNegro() {
    	manager.cargarBikesRedBlackBST();
    }
    
    public int size()
    {
    	return manager.getTrips().size();
	}
  

    public static void requerimiento1(int ID)
    {
    	manager.getBicicletaById(ID);
    }
    public static void requerimiento2(int IDMin, int IDMax)
    {
    	manager.biciEnRango(IDMin, IDMax);
    }

  
}
