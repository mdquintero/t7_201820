/**
 * Autor: Mateo David Quintero Reyes
 */
package model.data_structures;

import java.util.Iterator;

/**
 * Implementacion de una lista generica doblemente encadenada
 * Esta implementacion se hizo en base al material trabajado en clase
 * @param <T> Objeto generico
 */
public class LinkedList <T> implements List<T>{

	//ATRIBUTOS

	private Node<T> frst;

	private Node<T> last;

	private int listSize;



	//CONSTRUCTORES

	public LinkedList(){
		frst=null;
	}

	public LinkedList(T item){
		frst=new Node<T>(item, null, null);
		listSize=1;
	}

	//METODOS


	@Override
	public void addFirst(T item) {

		Node tmp = new Node(item, frst, null);
		if(frst != null ) {frst.setPrev(tmp);}
		frst = tmp;
		listSize++;

	}

	@Override
	public void removeFirst() {
		if (listSize == 0) {
			System.out.println("La lista esta vacia");
			return;
		}
		Node tmp = frst;
		frst = frst.getNext();
		frst.setPrev(null);
		listSize--;


	}

	@Override
	public void add(T item) {

		Node <T> newNode= new Node<>(item, null, null);

		if(isEmpty()){
			frst = newNode;
			last=frst;
			listSize++;
		}

		else{

			last.setNextNode(newNode);
			newNode.setPrev(last);
			last=newNode;
			listSize++;
		}


	}

	@Override
	public void remove(int pos) {

		if(!isEmpty()){
			if(pos == 0){
				Node<T> eliminado = frst;
				frst=frst.getNext();
				eliminado=null;
				listSize--;
			}
			else{
				Node<T> eliminado= get(pos);
				eliminado.getPrev().setNextNode(eliminado.getNext());
				eliminado.getNext().setPrev(eliminado.getPrev());
				eliminado=null;
				listSize--;
			}
		}

	}

	@Override
	public Node<T> get(int pos) {

		Node<T> found = frst;


		if(isEmpty())
		{
			System.out.println("La lista no contiene elementos");
			return null;
		}

		else if(pos == 1){
			return found;
		}

		else{
			for(int i = 2; i <= listSize; i++)
			{
				found = frst.getNext();
				if(i == pos)
				{
					return found;
				}
				
			}
		}
		return  found;
	}

	@Override
	public int size() {
		return listSize;
	}

	@Override
	public boolean isEmpty() {
		return listSize==0;
	}

	public Iterator<T> iterator(){
		ListIterator<T> listIt = new ListIterator<T>();
		listIt.setCurrent(frst);
		return listIt;
		
	}

	}

