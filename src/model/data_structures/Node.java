package model.data_structures;

public class Node<T> {
	
private Node<T> next;
	
	private Node<T> prev;
	
	private T item;
	
	public Node(T item, Node<T> pnext, Node<T> pprev) {
        this.item = item;
        this.next = pnext;
        this.prev = pprev;
    }
	
	public Node<T> getNext(){
		return next;
	}
	
	public void setNextNode(Node<T> next){
		this.next=next;
	}
	
	public T getItem(){
		return item;
	}
	
	public void setItem(T item){
		this.item=item;
	}

	public Node<T> getPrev() {
		return prev;
	}

	public void setPrev(Node<T> prev) {
		this.prev = prev;
	}
	
	

}
