package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.lang.Math;
import com.opencsv.CSVReader;

import API.IManager;
import model.data_structures.LinkedList;
import model.data_structures.ListIterator;
import model.data_structures.RedBlackBST;
import model.data_structures.Sorting;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;
import model.vo.Trip.Tipo;

public class Manager<K extends Comparable<K>, V> implements IManager {

	public final static String rutaGeneral="."+File.separator+"data"+File.separator;
	//Ruta del archivo de datos 2017-Q1
	// TODO Actualizar
	public static final String TRIPS_Q1 = rutaGeneral+"Divvy_Trips_2017_Q1.csv";

	//Ruta del archivo de trips 2017-Q2
	// TODO Actualizar	
	public static final String TRIPS_Q2 = rutaGeneral+"Divvy_Trips_2017_Q2.csv";

	//Ruta del archivo de trips 2017-Q3
	// TODO Actualizar	
	public static final String TRIPS_Q3 = rutaGeneral+"Divvy_Trips_2017_Q3.csv";

	//Ruta del archivo de trips 2017-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q4 = rutaGeneral+"Divvy_Trips_2017_Q4.csv";

	//Ruta del archivo de trips 2017-Q1-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q1_Q4 = "Ruta Trips 2017-Q1-Q4 en directorio data";

	//Ruta del archivo de stations 2017-Q1-Q2
	// TODO Actualizar	
	public static final String STATIONS_Q1_Q2 = rutaGeneral+"Divvy_Stations_2017_Q1Q2.csv";

	//Ruta del archivo de stations 2017-Q3-Q4
	// TODO Actualizar	
	public static final String STATIONS_Q3_Q4 = rutaGeneral+"Divvy_Stations_2017_Q3Q4.csv";

	private static LinkedList<Trip> linkTrips = new LinkedList<Trip>();

	private static LinkedList<Bike> listaBikes = new LinkedList<Bike>();

	private static LinkedList<Station> listaEncadenadaStations= new LinkedList<Station>();
	
	private Trip[] arregloTrips;
	
	private RedBlackBST<Integer, Bike> arbolBikes;


	public LinkedList<Trip> getTrips()
	{
		return linkTrips;
	}

	//M�TODOS DE CARGA

	/**
	 * Carga las estaciones por par�metro
	 */
	public void cargarStations(String rutaStations) {
		//cargar stations
		try{
			FileReader fr = new FileReader(new File(rutaStations));
			CSVReader br = new CSVReader(fr);

			String[] line = br.readNext();
			//Se avanza de nuevo porque la l�nea anterior no contiene datos
			line = br.readNext();

			while(line!=null)
			{
				String[] fecha = (line[6].replace(' ', '/').replace(':', '/')).split("/");
				int tamano = fecha.length;
				Integer[]fecha2 = new Integer[tamano];
				for(int i = 0; i< fecha2.length; i++)
				{
					fecha2[i] = Integer.parseInt(fecha[i]);
				}
				LocalDateTime ini = LocalDateTime.of(fecha2[2], fecha2[0], fecha2[1],fecha2[3],fecha2[4],tamano==6 ? fecha2[5]:00);
				Station s = new Station(Integer.parseInt(line[0]), line[1],ini, Double.parseDouble(line[3]),Double.parseDouble(line[4]));


				listaEncadenadaStations.add(s);

				line = br.readNext();
			}

			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	
	/**
	 * Carga todo
	 * @param rutaTrips la ruta
	 */
	public void cargar(String rutaTrips) {

		//cargar trips
		try{
			FileReader fr = new FileReader(new File(rutaTrips));
			CSVReader br = new CSVReader(fr);
			Trip t=null;


			String[] line = br.readNext();
			//Se avanza de nuevo porque la l�nea anterior no contiene datos
			line = br.readNext();
			while(line!=null)
			{


				String[] fechaInicio = (line[1].replace(' ', '/').replace(':', '/')).split("/");
				int tamano = fechaInicio.length;
				Integer[]fechaInicio2 = new Integer[tamano];
				String[] fechaFin = (line[2].replace(' ', '/').replace(':', '/')).split("/");
				Integer[]fechaFin2 = new Integer[tamano];

				for(int i = 0; i< fechaFin2.length; i++)
				{
					fechaInicio2[i] = Integer.parseInt(fechaInicio[i]);
					fechaFin2[i] = Integer.parseInt(fechaFin[i]);
				}

				LocalDateTime ini = LocalDateTime.of(fechaInicio2[2], fechaInicio2[0], fechaInicio2[1],fechaInicio2[3],fechaInicio2[4],(tamano==6 ? fechaInicio2[5]:00));
				LocalDateTime fini = LocalDateTime.of(fechaFin2[2], fechaFin2[0], fechaFin2[1],fechaFin2[3],fechaFin2[4],tamano==6 ? fechaFin2[5]:00);
				if(line[10]==null || line[10].equals(""))
				{
					t = new Trip(Integer.parseInt(line[0].replace("\"","")),  ini , fini , Integer.parseInt(line[3].replace("\"","")), 
							Integer.parseInt(line[4].replace("\"","")), Integer.parseInt(line[5].replace("\"","")), Integer.parseInt(line[7].replace("\"","")), Trip.UNKNOWN);
					
					double dist = calcularDistancia(t.getStartStationId(), t.getEndStationId());
					t.setDistance(dist);
				}
				else{
					t = new Trip(Integer.parseInt(line[0].replace("\"","")),  ini , fini , Integer.parseInt(line[3].replace("\"","")), 
							Integer.parseInt(line[4].replace("\"","")), Integer.parseInt(line[5].replace("\"","")), Integer.parseInt(line[7].replace("\"","")), line[10]);
					
					double dist = calcularDistancia(t.getStartStationId(), t.getEndStationId());
					t.setDistance(dist);
					
				}
				linkTrips.add(t);
				line = br.readNext();
			}
			br.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}

	}

	/**
	 * Permite cargar una muestra de los viajes existentes en un periodo de tiempo, incluyendo los l�mites
	 * @param fechaInicial fecha inicial del periodo de tiempo
	 * @param fechaFinal fecha final del periodo de tiempo
	 * @return Lista encadenada con viajes dentro de un periodo de tiempo.
	 */ 
	public static LinkedList<Trip> ViajesEnPeriodoDeTiempo(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		LocalDateTime inicio = fechaInicial.minusNanos(1);
		LocalDateTime fin = fechaFinal.plusNanos(1);
		LinkedList<Trip> r = new LinkedList<Trip>();
		ListIterator<Trip> listIte = (ListIterator<Trip>) linkTrips.iterator();
		while(listIte.hasNext())
		{
			Trip actual = listIte.next();
			LocalDateTime inicioActual = actual.getStartTime();
			LocalDateTime finActual = actual.getStopTime();
			if(inicioActual.isAfter(inicio)&&finActual.isBefore(fin))
			{
				r.add(actual);
			}
		}
		return r; 

	}

	public void cargarBikesRedBlackBST() {
		Trip[] arreglo = tripsToArray();
		
		if(arbolBikes==null)
			arbolBikes = new RedBlackBST<>();
		
		if(arreglo!=null) {
			Sorting.ordenar(Sorting.QUICK, arreglo);
			
			int idActualBike =0;
			int cantViajes =0;
			double distancia=0.0;
			int duracion=0;
			int i =0;
			
			boolean cen = false;
			
			while(i<arreglo.length) {
				
				Trip actual = arreglo[i];
				
				idActualBike=actual.getBikeId();
				
				while(actual.getBikeId()==idActualBike && !cen) {
					
					cantViajes++;
					distancia+=actual.getDistance();
					duracion+=actual.getTripDuration();
					
					i++;
					
					boolean alFinal = (i==arreglo.length);
					if(!alFinal)
						actual=arreglo[i];
					
					else {
						actual=arreglo[i-1];
						cen=true;
					}
				}
				
				arbolBikes.put(idActualBike, new Bike(idActualBike, cantViajes, distancia, duracion));
				
				cantViajes=0;
				distancia=0.0;
				duracion=0;
			}
		}
		//numero de bicicletas
		System.out.println("El numero de bikes es " + arbolBikes.size());
		//altura del �rbol
		System.out.println("La altura del �rbol es " + arbolBikes.height());
		//altura promedio del �rbol
		System.out.println("La altura promedio del �rbol es " +darAlturaPromedio());
	}

	//METODOS AUXILIARES

	public double darAlturaPromedio() {
		int x = 0;
		x=arbolBikes.darTotBusq();
		return (double) (x/arbolBikes.size());
		
	}
	/**
	 * Crea un arreglo con todos los trips almacenados al cargar.
	 * @return Arreglo con todos los trips almacenados al cargar.
	 */
	public Trip[] tripsToArray() {
		int tam = linkTrips.size();
		arregloTrips = new Trip[tam];
		
		ListIterator<Trip> iter = (ListIterator<Trip>) linkTrips.iterator();
		
		int i=0;
		while(iter.hasNext()) {
			arregloTrips[i] = iter.next();
			i++;
		}
		
		return arregloTrips;
	}

	/**
	 * Calcula la distancia entre dos estaciones
	 * @param inicio Id estacion inicial
	 * @param end Id estacion final
	 * @return Distancia entre dos estaciones
	 */
	public static double calcularDistancia(int inicio, int end)
	{
		boolean ini = false;
		boolean fini = false;
		Station referencia = null;
		Station ended = null;
		boolean yatodo=false;

		double x = 0;

		ListIterator<Station> listite = (ListIterator<Station>) listaEncadenadaStations.iterator();

		while(listite.hasNext()&&!yatodo) {

			Station s = listite.next();
			if(s.getStationId()==inicio)
			{
				referencia = s;
				ini = true;
			}
			if(s.getStationId()==end)
			{
				ended = s;
				fini = true;
			}
			if(ini&&fini) {
				yatodo=true;
			}
		}
		if(referencia != null && ended != null)
		{
			x = calculateDistance(ended.getLat(), ended.getLongitude(), referencia.getLongitude(), referencia.getLat());
		}

		return x;
	}

	public void getBicicletaById(int ID)
	{
		Bike bici = arbolBikes.get(ID);
		if(bici != null)
		{
			System.out.println("ID Bicicleta:" + ID + "\n Total de viajes: " + bici.getTotalTrips() + "\n Distancia recorrida: " + bici.getTotalDistance() + "\n Tiempo total: " + bici.getTotalDuration());
		}
		else 
		{
			System.out.println("No se pudo encontrar la bicicleta identificada con el ID " + ID);
		}
	}

	public void biciEnRango(int IDMin, int IDMax)
	{
		for(int i = IDMin; i<= IDMax; i++)
		{
			getBicicletaById(i);
		}
	}

	//METODO CREADO POR Maria Jose Ocampo Vargas
	//METODO CREADO POR Maria Jose Ocampo Vargas
	//METODO CREADO POR Maria Jose Ocampo Vargas

	public static double calculateDistance (double lat1, double lon1, double longitudReferencia, double latitudReferencia) {

		final int R = 6371*1000; // Radious of the earth in meters 

		double latDistance = Math.toRadians(latitudReferencia-lat1); 

		double lonDistance = Math.toRadians(longitudReferencia-lon1); 

		double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat1))* Math.cos(Math.toRadians(latitudReferencia)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2); 

		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 

		double distance = R * c;

		return distance; 

	}



}
