package model.vo;

import java.util.Iterator;

public class Bike implements Comparable<Bike>{

    private int bikeId;
    private int totalTrips;
    private double totalDistance;
    private int totalDuration;

    public Bike(int bikeId, int totalTrips, double distancia, int ptotalDuration) {
        this.bikeId = bikeId;
        this.totalTrips = totalTrips;
        this.totalDistance = distancia;
        this.totalDuration = ptotalDuration;
    }

    @Override
    public int compareTo(Bike o) {
    	if(this.bikeId>o.getBikeId())
    		return 1;
    	else if(this.bikeId<o.getBikeId())
    		return -1;
    	else
    		return 0;
    }
    
    public int compareToByDistances(Bike o) {
    	if(this.totalDistance>o.getTotalDistance())
    		return -1;
    	else
    		return 1;
    }
    
    public int compareToByDurations(Bike o) {
    	if(this.totalDuration>o.getTotalDuration())
    		return -1;
    	else
    		return 1;
    }

    public int getBikeId() {
        return bikeId;
    }

    public int getTotalTrips() {
        return totalTrips;
    }

    public double getTotalDistance() {
        return totalDistance;
    }
    public int getTotalDuration() {
    	return totalDuration;
    }
    
    public void addDistance(double d)
    {
    	totalDistance+=d;
    }
    
    public void addTrips(int t)
    {
    	totalTrips+=t;
    }
    
    public void addDuration(double t)
    {
    	totalDuration+=t;
    }
}
